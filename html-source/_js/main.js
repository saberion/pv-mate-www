window.scrollTo(0, 0);

window.onorientationchange = function() {
  window.location.reload();
};

if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        }
        return this;
    }
}

var media_width, media_height;
var isMacLike = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i)?true:false;
var isIOS = navigator.platform.match(/(iPhone|iPod|iPad)/i)?true:false;
var isTouchDevice = 'ontouchstart' in document.documentElement;
var tablet_width = 1024;
var mobile_width = 768;


var homeSlider;

var is_scrollbar_init = false;
//var infobox_top = 0;

$(document).ready(function () {

  media_width = $(window).width();
  media_height = $(window).height();

// When the window is resized
$(window).resize(function() {

  $('.with_text').css('height', $('.with_lg_img > img').height() );
  $('.about_img').css('height', $('.about_text').outerHeight() );
  $('.block_text').css('height', $('.block_img').outerHeight() );
  $('.right_pic').css('height', $('.left_blue').outerHeight() );
// Kick off one resize to fix all videos on page load
}).resize();

  var swiper = new Swiper('.home_slider', {
      slidesPerView: 1,
      paginationClickable: true,
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      keyboardControl: true,
      autoplay: 3500,
      autoplayDisableOnInteraction: false,
      speed: 1200,
  });
});

var swiper = new Swiper('.news_gallery', {
    pagination: '.swiper-pagination',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    slidesPerView: 1,
    spaceBetween: 0,
    autoplay: 3500,
    autoplayDisableOnInteraction: false,
    loop: true,
});

$(window).resize(function () {
    $('.something').css('height', $(window).height());
});
$(function(){ $(window).resize() });
$(window).scroll(function() {
    if ($(window).scrollTop() >= 100) {
        $('.navbar ').addClass('bgwhite');
    } else {
        $('.navbar ').removeClass('bgwhite');
    }
});

var open = 'glyphicon-eye-open';
var close = 'glyphicon-eye-close';
var ele = document.getElementById('password');

document.getElementById('toggleBtn').onclick = function() {
  if( this.classList.contains(open) ) {
    ele.type="text";
    this.classList.remove(open);
    this.className += ' '+close;
  } else {
    ele.type="password";
    this.classList.remove(close);
    this.className += ' '+open;
  }
}


$(function() {

  //show/hide password function initialize
  showHidePassword();

  //register popup
  registerModal();

  //supplier slider
  supplierSlider();

  //video popup
  videoPopup();

});

//show/hide password function
function showHidePassword() {
  $('.show-password').click(function(e) {
    e.preventDefault();

    $('.fa-not', this).toggleClass('fa-eye-slash fa-eye');

    var input = $(this).siblings('.password-field');

    if(input.attr('type') === 'password') {
      input.attr('type', 'text');
    } else {
      input.attr('type', 'password');;
    }
  });
}

//register popup
function registerModal() {
  $('.register').click(function() {
    var role = $(this).data('role');
    var rolevalue = $()

    if(role == 'retailer') {
      $('.tab-role').removeClass('active');
      $('.retailer-tab').addClass('active');
    } else if(role == 'installer') {
      $('.tab-role').removeClass('active');
      $('.installer-tab').addClass('active');      
    }

  });
}

//supplier slider
function supplierSlider() {
  var owl = $('.suppliers-slider');

  owl.owlCarousel({
    items: 4,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    padding: 0,
    margin: 0,
    autoplay: false,
    loop: true,
    responsive:{
        0:{
            items:2
        },
        480:{
            items:3
        },        
        768:{
            items:4
        },
        992: {
          items: 5
        }
    }   
  });
}

//video popup
function videoPopup() {

  var videoPlayer = $('.video-popup-player');
  var popup = $('.video-popup');

  const player = new Plyr(videoPlayer);

  $('.video-play').click(function(e) {
    e.preventDefault();

    var url = $(this).attr('href');

    console.log(url);

    var videoId = YouTubeGetID(url);
    //var videoId = $(this).attr('video-id');

    console.log(videoId);

    //videoPlayer.attr('data-plyr-embed-id', videoId);

    popup.css({ 'display': 'flex' });

    player.source = {
        type: 'video',
        sources: [
            {
                src: videoId,
                provider: 'youtube',
            },
        ],
    };

  }); 

  $('.popup-close').click(function(e) {
    e.preventDefault();

    popup.hide();
    player.stop();

  }); 
}

//get youtube id
function YouTubeGetID(url){
  var ID = '';
  url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
  if(url[2] !== undefined) {
    ID = url[2].split(/[^0-9a-z_\-]/i);
    ID = ID[0];
  }
  else {
    ID = url;
  }
    return ID;
}
